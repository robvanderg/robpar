package test;
// ~/Documents/jaar6/robpar/src/test/email/source/source_text_ascii_tokenized $ sed -i 's/<.*>//g' *
// ~/Documents/jaar6/robpar/src/test/email/penntree $ sed -i 's/^(/(TOP/g' *

import earley.CombinedParser;
import earley.State;
import grammar.Grammar;
import grammar.GrammarReader;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import tagger.TagReader;

public class Test {
	public static void main(String[] args) {
		//testGen();
		//testParse();
		//testTagData();
		getProbTreebank("data/train/all_train");
		getProbTreebank("data/dev/twitterdev_gold");
	}

	public static void getProbTreebank(String treebank){
		Grammar myGrammar= new GrammarReader().generate(new File(treebank));
		int[][] test = myGrammar.getRulesForCat(myGrammar.toId("NP"));
		for (int[] rule: test){
			if(rule.length == 4 && rule[2] == myGrammar.toId("DT") && rule[3] == myGrammar.toId("NN")){
				for (int idx = 2; idx < rule.length; idx++){
					System.out.print(myGrammar.toConstituent(rule[idx]) + " - ");
				}
				System.out.println();
				System.out.println(rule[0] + "\t" + rule[1]);
				System.out.println(State.toProb(State.toProb(rule[0], rule[1])));
			}
		}
	}



	public static void testTagData(){
		Grammar myGrammar = new Grammar("gram/ewt.ser");

		HashMap<String, int[][]> posData = new TagReader(myGrammar.getConsts(), new File("data/train/all_train")).getPosData();
		System.out.println(posData.size());
		System.out.println(posData.get("nice").length);
		for(int[] niceData: posData.get("nice")){
			System.out.println(myGrammar.toConstituent(niceData[0]) + "\t" + niceData[1]);
		}
	}

	public static void testParse(){
		File folder = new File("src/test/email/penntree");
		File[] files = folder.listFiles();
		File[] trainFiles = new File[files.length-3];
		System.arraycopy(files, 0, trainFiles, 0, files.length-3);
		File[] testFiles = new File[3];
		System.arraycopy(files, files.length-3, testFiles, 0, 3);

		Grammar myGrammar= new GrammarReader().generate(trainFiles);	


		ArrayList<String> pos = new ArrayList<String>();
		pos.add("NNP");
		pos.add("UH");
		pos.add(".");

		CombinedParser myParser = new CombinedParser(myGrammar);

		myParser.parse("Username whooh .", pos);

	}

	public static void testGen(){
		File folder = new File("src/test/email/penntree");
		File[] files = folder.listFiles();
		File[] trainFiles = new File[files.length-3];
		System.arraycopy(files, 0, trainFiles, 0, files.length-3);
		File[] testFiles = new File[3];
		System.arraycopy(files, files.length-3, testFiles, 0, 3);

		Grammar myGrammar= new GrammarReader().generate(trainFiles);	
		int[][] sRules = myGrammar.getRulesForCat(myGrammar.toId("TOP"));
		for (int[] rule: sRules){
			System.out.print(rule[0] + " " + rule[1] + "  \t");
			for (int constIdx = 2; constIdx < rule.length; constIdx++){
				System.out.print(myGrammar.toConstituent(rule[constIdx]) + ' ');
			}
			System.out.println();
		}
	}
}
