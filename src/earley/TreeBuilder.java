package earley;

import grammar.Grammar;

import java.util.ArrayList;

public class TreeBuilder {
	private Grammar myGrammar;

	public TreeBuilder(Grammar myGrammar){
		this.myGrammar = myGrammar;
	}

	private String[] sent;
	private ArrayList<int[]> chart[];

	public void printChartSizes(){
		for (int chartIndex = 0; chartIndex < chart.length; chartIndex++)
			System.out.println(chartIndex + "\t" + chart[chartIndex].size());
	}
	
	public String getBestParse(ArrayList<int[]> chart[], String sentence){
		this.sent = sentence.split(" ");
		this.chart = chart;
		
		String bestParse = "";
		double highestProb = 0.0;
		
		for (int[] state: chart[chart.length -1]){
			if (state[State.LHS] == myGrammar.toId("TOP") &&
			state[State.RECEND] == state[State.PREDEND] &&
			state[State.BEGIN] == 0 &&
			state[State.END] == chart.length -1
					){
				if(State.toProb(state[State.TREEPROB]) > highestProb){
					wordCounter = 0;
					totalString = "";
					prob = 1.0;
					printBracket(state);
					bestParse = (totalString.substring(1,totalString.length()));
					highestProb = State.toProb(state[State.TREEPROB]);
				}
			}
		}
		return bestParse;
	}
	
	public ArrayList<String> getAllParses(ArrayList<int[]> chart[], String sentence){
		ArrayList<String> foundParses = new ArrayList<String>();

		this.sent = sentence.split(" ");
		this.chart = chart;
		
		for (int[] state: chart[chart.length -1]){
			if (state[State.LHS] == myGrammar.toId("TOP") &&
			state[State.RECEND] == state[State.PREDEND] &&
			state[State.BEGIN] == 0 &&
			state[State.END] == chart.length -1
					){
				wordCounter = 0;
				totalString = "";
				prob = 1.0;
				printBracket(state);
				foundParses.add(totalString.substring(1,totalString.length()));
			}
		}
		return foundParses;
	}
	
	@SuppressWarnings("unused")
	private double prob; //TODO, these three variables should be built in the method
	
	private int wordCounter;
	private String totalString;
	
	private void printBracket(int[] state){
		prob *= ((float)state[State.RHSCOUNT] / state[State.LHSCOUNT]);
		totalString += " (" + myGrammar.toConstituent(state[State.LHS]);
				
		if(myGrammar.isPos(state[State.LHS]))
			totalString += " " + sent[wordCounter++]; 
		for(int srcIndex = state.length-1; srcIndex > state[State.PREDEND]; srcIndex--)
			if (state[srcIndex] != 0)
				printBracket(this.getState(state[srcIndex]));
		totalString += ")";
	}
	
	private int[] getState(int id) {
		for (ArrayList<int[]> chartItem: chart)
			for (int[] state: chartItem)
				if (state[State.ID] == id)
					return state;
		System.err.println("err: state not found: " + id);
		return null;
	}
}