package earley;

public class State {
	public final static int ID = 0;
	public final static int LHSCOUNT = 1;
	public final static int RHSCOUNT = 2;
	public final static int TREEPROB = 3;
	public final static int BEGIN = 4;
	public final static int END = 5;
	public final static int RECEND = 6;
	public final static int PREDEND = 7;
	public final static int LHS = 8;
	public final static int RHS = 9;

	public static int[] toState(int begin, int end, int  lhs, int[] recognised, int[] predicted, int treeProb, int lhsCount, int rhsCount){		
		int[] state = new int[RHS + recognised.length + predicted.length + 10]; //TODO, fix the static size of the number of sources! (10)
		state[ID] = 0;
		state[BEGIN] = begin;
		state[END] = end;
		state[LHSCOUNT] = lhsCount;
		state[RHSCOUNT] = rhsCount;
		state[TREEPROB] = treeProb;
		state[RECEND] = recognised.length + RHS;
		state[PREDEND] = predicted.length + state[RECEND];
		state[LHS] = lhs;
		System.arraycopy(recognised, 0, state, RHS, recognised.length);
		System.arraycopy(predicted, 0, state, state[RECEND], predicted.length);
		for (int i = state[PREDEND]+ 1; i < state.length; i++)
			state[i] = 0;
		return state;
	}
	
	public static int toProb(double prob){
		return (int)(-Math.log(prob) * 1E3);
	}
	
	public static int toProb (int lhs, int rhs){
		return (int)(-Math.log(rhs / (float)lhs ) * 1E3);
	}
	
	public static double toProb (int intProb){
		return 1/Math.exp(intProb / (float)(1E3));
	}

}
