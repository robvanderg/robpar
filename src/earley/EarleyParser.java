package earley;

import grammar.Grammar;

import java.util.ArrayList;
import java.util.HashMap;

public class EarleyParser {
	// Variables used by the parser
	private Grammar myGrammar;

	// variables used for each parse
	private int[][][] posCands;

	private ArrayList<int[]> chart[];
	private HashMap<ArrayList<Integer>, int[]> foundStates;
	private ArrayList<int[]> stateIndexer;

	private int stateId = 0;
	private int treshold = Integer.MAX_VALUE; 
	
	public EarleyParser(Grammar myGrammar) {
		this.myGrammar = myGrammar;
	}

	public void setTreshold(double tresholdProb) {
		this.treshold = -(int)(Math.log(tresholdProb) *1E7);
	}

	@SuppressWarnings("unchecked") // for unchecked conversion in chart, because of weird data structure
	public ArrayList<int[]> parse(int[][][]posCands)[] {

			this.posCands = posCands;

			this.chart = new ArrayList[posCands.length + 1];
			for (int chartIndex = 0; chartIndex < chart.length; chartIndex++){
				this.chart[chartIndex] = new ArrayList<int[]>();
			}
			this.foundStates = new HashMap<ArrayList<Integer>, int[]>();
			this.stateIndexer = new ArrayList<int[]>();

			int startState[] = State.toState(0, 0, myGrammar.toId("Y"), new int[0], new int[]{myGrammar.toId("TOP")}, 0,1,1); 
			this.enqueue(startState, 0, new int[0]);

			for (int chartIndex = 0; chartIndex < chart.length; chartIndex++){
				for(int stateIndex = 0; chart[chartIndex].size() > stateIndex; stateIndex++){
					int[] itrState = chart[chartIndex].get(stateIndex);
					if (!this.complete(itrState) && itrState[State.END] < this.posCands.length){
						if(!myGrammar.isPos(itrState[itrState[State.RECEND]])){
							this.predictor(itrState);
						}
						else{
							this.scanner(itrState, this.posCands[itrState[State.END]]);
						}
					}
					else{
						this.completer(itrState, chartIndex);
					}
				}
			}
			return chart;
	}
	//TODO, return a chart with only completed states!, with this (states from completer), evt ook scanner(?)
	// all possible trees can be restructed.

	private boolean complete(int[] itrState) {
		return itrState[State.RECEND] == itrState[State.PREDEND];
	}

	private void predictor(int[] curState){
		for (int[] rule: this.myGrammar.getRulesForCat(curState[curState[State.RECEND]])){
			if (rule[0] < treshold){
				int rhs[] = new int[rule.length - 2];
				System.arraycopy(rule, 2, rhs, 0, rule.length - 2);
				int[] addState = State.toState(curState[State.END], curState[State.END], curState[curState[State.RECEND]], 
						new int[0], rhs, State.toProb(rule[0], rule[1]), rule[0], rule[1]);
				this.enqueue(addState, curState[State.END], new int[0]);
			}
		}
	}

	private void scanner(int[] curState, int[][] posCandsToken){
		for (int[] posCand: posCandsToken){
			if (posCand[0] == curState[curState[State.RECEND]] && State.toProb(posCand[1]) > 0.1 ){
				int[] addState = State.toState(curState[State.END], curState[State.END] + 1, curState[curState[State.RECEND]], 
						new int[]{99999}, new int[0], 0, 1, 1);
				this.enqueue(addState, curState[State.END] + 1, new int[0]);
				break;
			}
		}
	}

	private void completer(int[] curState, int chartIndex){
		for (int[] itrState: chart[curState[State.BEGIN]]){
			if (itrState[State.END] == curState[State.BEGIN] &&
					itrState[State.PREDEND] > itrState[State.RECEND] &&
					curState[State.BEGIN] != curState[State.END] &&
					itrState[itrState[State.RECEND]] == curState[State.LHS]
					){
				int[] addState = itrState.clone();
				addState[State.END] = curState[State.END];
				addState[State.RECEND] = addState[State.RECEND] + 1;

				int prob = curState[State.TREEPROB] + State.toProb(addState[State.LHSCOUNT], addState[State.RHSCOUNT]);
				addState[addState[State.PREDEND] + 1] = curState[State.ID]; // to save tree 
				for(int srcIndex = itrState[State.PREDEND] + 1; srcIndex < itrState.length - 1; srcIndex++){
					if(itrState[srcIndex] != 0){
						addState[srcIndex + 1] = itrState[srcIndex];
						int multiplyProb = getState(itrState[srcIndex])[State.TREEPROB];
						canAdd(prob, multiplyProb);
						prob += multiplyProb;
					}
				}
				addState[State.TREEPROB] = prob;
				this.enqueue(addState, curState[State.END], new int[0]);
			}
		}
	}

	private void enqueue(int[] addState, int chartIndex, int[] sources){
		ArrayList<Integer> addStateForList = new ArrayList<Integer>();
		for (int stateInt = State.BEGIN; stateInt < addState[State.PREDEND]; stateInt++)
			addStateForList.add(addState[stateInt]);

		if(!this.foundStates.containsKey(addStateForList)){
			stateIndexer.add(new int[]{chartIndex, this.chart[chartIndex].size()});
			addState[State.ID] = this.stateId;
			this.chart[chartIndex].add(addState);
			this.foundStates.put(addStateForList,  new int[]{stateId, addState[State.TREEPROB]});
			stateId++;
		}
		else if (this.foundStates.get(addStateForList)[1] > addState[State.TREEPROB]){
			int id = this.foundStates.get(addStateForList)[0];
			
			int[] oldStateData = this.foundStates.get(addStateForList);
			addState[State.ID] = oldStateData[0];
			this.foundStates.put(addStateForList, new int[]{addState[State.ID], addState[State.TREEPROB]});
			chart[stateIndexer.get(id)[0]].set(stateIndexer.get(id)[1], addState);
		}
	}
	
	private int[] getState(int id) {
		return chart[stateIndexer.get(id)[0]].get(stateIndexer.get(id)[1]);
	}
	
	
	
	
	
	
	// Helper method to make sure no integer overflow happens
	public static boolean canAdd(int... values) {
		long longSum = 0;
		int intSum = 0;
		for (final int value: values) {
			intSum += value;
			longSum += value;
		}
		if(intSum != longSum){
			System.err.println("integer overflow!");
		}
		return intSum == longSum;
	}
	
	public String stateToString(int[] state){
		String totalString = state[State.ID] + "\t";
		totalString += myGrammar.toConstituent(state[State.LHS]) + " -> ";
		for (int i = State.RHS; i < state[State.RECEND]; i++)
			totalString += myGrammar.toConstituent(state[i]) + " ";
		totalString += "* ";
		for (int i = state[State.RECEND]; i < state[State.PREDEND]; i++)
			totalString += myGrammar.toConstituent(state[i]) + " ";
		totalString += "\t[" + state[State.BEGIN] + "," + state[State.END] +"]  ";

		for(int srcIndex = state[State.PREDEND] + 1; srcIndex < state.length - 1; srcIndex++){
			if(state[srcIndex] != 0){
				totalString += state[srcIndex] + ",";
			}
		}
		totalString += "\t " + State.toProb(state[State.TREEPROB]);
		return totalString;
	}
}