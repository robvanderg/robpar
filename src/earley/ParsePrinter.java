package earley;

public class ParsePrinter {
	public static void print(String parse) {
		int bracketCount = 0;
		String word = "";
		for(char c: parse.toCharArray()) {
			if (c == ' '){
				if (word.trim().length() > 0){
					for(int i = 1; i < bracketCount; i++)
						System.out.print("   ");
					System.out.println(word);
				}
				word = "";
			}
			else if(c == ')'){ //POS!!
				if (word.trim().length() > 0)
					for(int i = 1; i < bracketCount + 1; i++)
						System.out.print("   ");
					System.out.println(word);
				bracketCount--;
				word = "";
			}
			else if (c == '(')
				bracketCount++;
			else if(c != '\n')
				word += c;
		}
	}
}