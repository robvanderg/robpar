package earley;
import grammar.Grammar;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import tagger.PosTagger;
import tagger.TagReader;

public class CombinedParser {
	private Grammar myGrammar;
	private EarleyParser myParser;

	private TreeBuilder myPicker;
	
	private PosTagger myTagger;

	public CombinedParser(Grammar myGrammar){
		this.myGrammar = myGrammar;
		this.myPicker = new TreeBuilder(this.myGrammar);
		
		HashMap<String, int[][]> posData = new TagReader().read(new File("data/pos.ser"));
		
		this.myTagger = new PosTagger(posData, myGrammar.getConsts());
	}
	
	public String parse(String sentence) {
		this.myParser = new EarleyParser(this.myGrammar); //TODO FOR SOME REASON PRE INITIALIZING DOES NOT WORK ANYMORE

		int[][][] posData = this.myTagger.tag(sentence.split(" "), true);

		ArrayList<int[]> chart[] = this.myParser.parse(posData);

		return myPicker.getBestParse(chart, sentence);
	}

	public String parse(String sentence, ArrayList<String> pos) {
		this.myParser = new EarleyParser(this.myGrammar); //TODO FOR SOME REASON PRE INITIALIZING DOES NOT WORK ANYMORE

		int[][][] posData = new int[pos.size()][][];
		for (int wordIndex = 0; wordIndex < pos.size(); wordIndex++){
			posData[wordIndex] = new int[1][];
			posData[wordIndex][0] = new int[]{this.myGrammar.toId(pos.get(wordIndex)), 1, 1};
		}
		ArrayList<int[]> chart[] = this.myParser.parse(posData);

		return myPicker.getBestParse(chart, sentence);
	}
}
