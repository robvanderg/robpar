package tagger;

import java.util.ArrayList;
import java.util.HashMap;

public class OwnTagger {
	HashMap<String, int[][]> posData;

	
	public OwnTagger(HashMap<String, int[][]> posData, ArrayList<String> consts) {
		this.posData = posData;
	}

	public int[][][] tag(ArrayList<String> sentence) {
		int[][][] tagData = new int[sentence.size()][][];
		for(int wordIndex = 0; wordIndex < sentence.size(); wordIndex++){
			String word = sentence.get(wordIndex).toLowerCase();
			int[][] wordData;

			if (this.posData.containsKey(word)){
				wordData = this.posData.get(word);
			}
			else{
				wordData = this.posData.get("UNK");
			}
			
			tagData[wordIndex] = wordData;
		}
		return tagData;
	}
}
