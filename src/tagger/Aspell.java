package tagger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.swabunga.spell.engine.EditDistance;
import com.swabunga.spell.engine.SpellDictionaryHashMap;
import com.swabunga.spell.engine.Word;
import com.swabunga.spell.event.SpellChecker;

public class Aspell {
	private static SpellDictionaryHashMap dictionary = null;
	private static SpellChecker spellChecker = null;

	public Aspell(){
		try {
			dictionary = new SpellDictionaryHashMap(new File("data/dic/eng_com.dic"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		spellChecker = new SpellChecker(dictionary);
	}

	public ArrayList<String> getSuggestions(String word){
		ArrayList<String> returnList = new ArrayList<String>();
		for(Object itrCand: spellChecker.getSuggestions(word, 0))
			returnList.add(((Word)itrCand).toString());
		return returnList;
	}
	
	public int getDistance(String word1, String word2){
	    return EditDistance.getDistance(word1, word2);
	}
}