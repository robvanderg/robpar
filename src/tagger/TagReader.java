package tagger;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import earley.State;

public class TagReader {
	private ArrayList<String> consts;
	private HashMap<String, HashMap<Integer, Integer>> wordCounts;

	public TagReader() {
	}

	public TagReader(ArrayList<String> consts, File goldData) {
		this.wordCounts = new HashMap<String, HashMap<Integer, Integer>>();
		this.consts = consts;
		ArrayList<DepthItem> depthList = new ArrayList<DepthItem>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(goldData));
			int bracketCount = 0;
			int cInt;
			String word = "";
			while((cInt = br.read()) > 0) {
				char character = (char) cInt;
				//TODO make switch!
				if (character == ' '){
					if (word.trim().length() > 0)
						depthList.add(new DepthItem(bracketCount, word, false));
					word = "";
				}
				else if(character == ')'){ //POS!!
					if (word.trim().length() > 0)
						depthList.add(new DepthItem(bracketCount + 1, word, true));
					bracketCount--;
					if (bracketCount == 0){
						addRules(depthList);
						depthList = new ArrayList<DepthItem>();
					}
					word = "";
				}
				else if (character == '(')
					bracketCount++;
				else if(character != '\n')
					word += character;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addRules(ArrayList<DepthItem> depthList){
		for (int itemIdx = 0; itemIdx < depthList.size(); itemIdx++){
			if(depthList.get(itemIdx).isWord == true){
				addTagCount(depthList.get(itemIdx -1).tag, depthList.get(itemIdx).tag);
			}
		}
	}

	public void addTagCount(String tag, String word){
		tag = cleanCons(tag);
		word = word.toLowerCase(); //TODO, hier verliezen we precisie!
		if(tag.equals("-NONE-"))
			return;
		int tagId = consts.indexOf(tag);
		if(wordCounts.containsKey(word)){
			if(wordCounts.get(word).containsKey(tagId)){
				wordCounts.get(word).put(tagId, wordCounts.get(word).get(tagId) + 1);
			}
			else{
				wordCounts.get(word).put(tagId, 1);
			}
		}
		else{
			HashMap<Integer, Integer> wordCountList = new HashMap<Integer, Integer>();
			wordCountList.put(tagId, 1);
			wordCounts.put(word, wordCountList);
		}
	}

	public HashMap<String, int[][]> getPosData (){ 
		HashMap<String, int[][]> posData = new HashMap<String, int[][]>();
		for (String word: wordCounts.keySet()){
			HashMap<Integer, Integer> wordHashMap = wordCounts.get(word);
			int[][] wordData = new int[wordHashMap.size()][];
			int posIdx = 0;

			int wordOccurences = 0;
			for(int posId: wordHashMap.keySet()){
				wordOccurences += wordHashMap.get(posId);
			}

			for(int posId: wordHashMap.keySet()){
				wordData[posIdx++] = new int[]{posId, State.toProb(wordHashMap.get(posId)/ (float) wordOccurences)};
			}

			posData.put(word, wordData);
		}
		return getUnknownData(posData);
	}

	/* Clean a constituent for efficiency reasons */
	private String cleanCons(String cons){
		int dashLoc = cons.indexOf('-');
		if (dashLoc > 0)
			cons = cons.substring(0, dashLoc);
		int eqLoc = cons.indexOf('=');
		if (eqLoc > 0)
			cons = cons.substring(0, eqLoc);

		if (cons.equals("META") || cons.equals("EDITED"))
			return "X";
		else if (cons.equals("NFP"))
			return ":";
		else if (cons.equals("ADD"))
			return "NNP";
		else if (cons.equals("HYPH"))
			return ":";
		else if (cons.equals("NML"))
			return "NP";
		return cons;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, int[][]>  read(File dataFile){
		HashMap<String, int[][]> posData = new HashMap<String, int[][]>();
		try{
			ObjectInput input = new ObjectInputStream (new BufferedInputStream(new FileInputStream(dataFile)));
			posData = (HashMap<String, int[][]>)input.readObject();
			input.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return posData;
	}

	public void write(String filePath){
		try {
			ObjectOutput output = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filePath)));
			output.writeObject(this.getPosData());
			output.close();
			System.out.println(filePath + " written");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HashMap<String, int[][]> getUnknownData(HashMap<String, int[][]> posData){

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("data/dev/twitterdev_goldpos"));

			int[] posCounter = new int[100];
			String line = "";
			while ((line = br.readLine()) != null){
				if(line.trim().length() > 0){
					if(!posData.containsKey(line.split("\t")[0])){
						posCounter[this.consts.indexOf(line.split("\t")[1])]++;
					}
				}
			}

			ArrayList<int[]> filteredList = new ArrayList<int[]>();
			int totalCounter = 0;
			for (int test = 0; test < 100; test++){
				if(posCounter[test] > 10){
					totalCounter += posCounter[test];
					filteredList.add(new int[]{test, posCounter[test]});
				}
			}
			int[][] finalData = new int[filteredList.size()][];
			for (int posIndex = 0; posIndex < filteredList.size(); posIndex++){
				finalData[posIndex] = new int[]{filteredList.get(posIndex)[0], State.toProb(filteredList.get(posIndex)[1]/ (float)totalCounter)};
			}

			posData.put("UNK", finalData);
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return posData;
	}
}
class DepthItem{ 
	int depth;
	String tag;
	boolean isWord;
	public DepthItem(int depth, String tag, boolean isWord){
		this.depth = depth;
		this.tag = tag;
		this.isWord = isWord;
	}
}