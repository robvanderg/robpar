package tagger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import tagger.ArkTagger.TaggedToken;
import earley.State;

public class PosTagger {
	private ArrayList<String> consts;

	private ArkTagger arkTagger;
	private OwnTagger ownTagger;

	public PosTagger(HashMap<String, int[][]> posData, ArrayList<String> consts) {
		this.consts = consts;
		this.arkTagger = new ArkTagger();
		this.ownTagger = new OwnTagger(posData, consts);
	}

	public int[][][] tag(String[] sent, boolean useOwnTagger){
		int[][][] returnArray = new int[sent.length][][];
		ArrayList<TaggedToken> arkTags = arkTagger.tag(new ArrayList<String>(Arrays.asList(sent)));
		

		for (int wordIndex = 0; wordIndex < sent.length; wordIndex++){
			int[][] wordData;
			wordData = new int[1][];
			if (useOwnTagger)
				wordData[0] = new int[]{consts.indexOf(arkTags.get(wordIndex).tag), State.toProb(arkTags.get(wordIndex).conf)};
			else
				wordData[0] = new int[]{consts.indexOf(arkTags.get(wordIndex).tag), 1};
			returnArray[wordIndex] = wordData;
		}

		if(useOwnTagger){
			int[][][] ownTags = ownTagger.tag(new ArrayList<String>(Arrays.asList(sent)));
			
			for (int wordIndex = 0; wordIndex < sent.length; wordIndex++){
				if(State.toProb(returnArray[wordIndex][0][1]) < 0.9){
					ArrayList<int[]> wordData = new ArrayList<int[]>();		

					wordData.add(returnArray[wordIndex][0]);

					for(int[] posData: ownTags[wordIndex]){
						if(posData[0] == wordData.get(0)[0]){ // same result as ark tagger
							wordData.set(0, new int[]{wordData.get(0)[0] ,State.toProb(State.toProb(wordData.get(0)[1]) + State.toProb(posData[1]))});
						}
						else{
							int prob = State.toProb((1 - State.toProb(wordData.get(0)[1])) * State.toProb(posData[1]));
							wordData.add(new int[]{posData[0], prob});
						}
					}
					
					returnArray[wordIndex] = new int[wordData.size()][];
					for (int posIndex = 0; posIndex < returnArray[wordIndex].length; posIndex++){
						returnArray[wordIndex][posIndex] = wordData.get(posIndex);
					}
				}

			}
		}

		return returnArray;
	}
}