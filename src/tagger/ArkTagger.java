package tagger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cmu.arktweetnlp.impl.Model;
import cmu.arktweetnlp.impl.ModelSentence;
import cmu.arktweetnlp.impl.Sentence;
import cmu.arktweetnlp.impl.features.FeatureExtractor;


/**
 * Tagger object -- wraps up the entire tagger for easy usage from Java.
 * 
 * To use:
 * 
 * (1) call loadModel().
 * 
 * (2) call tokenizeAndTag() for every tweet.
 *  
 * See main() for example code.
 * 
 * (Note RunTagger.java has a more sophisticated runner.
 * This class is intended to be easiest to use in other applications.)
 */
public class ArkTagger {
	private Model model;
	private FeatureExtractor featureExtractor;

	public ArkTagger(){
		String modelFilename = "src/tagger/model.ritter_ptb_alldata_fixed.20130723";
		this.loadModel(modelFilename);
	}

	public void loadModel(String modelFilename) {
		try {
			this.model = Model.loadModelFromText(modelFilename);
			this.featureExtractor = new FeatureExtractor(model, false);
		} catch (IOException e) {
			System.err.println("Problem with loading the tagger model: " + modelFilename);
			e.printStackTrace();
		}
	}

	public ArrayList<TaggedToken> tag(List<String> tokens) {
		Sentence sentence = new Sentence();
		sentence.tokens = tokens;
		ModelSentence ms = new ModelSentence(sentence.T());
		
		featureExtractor.computeFeatures(sentence, ms);
		model.greedyDecode(ms, true);

		ArrayList<TaggedToken> taggedTokens = new ArrayList<TaggedToken>();

		for (int t=0; t < sentence.T(); t++) {
			TaggedToken tt = new TaggedToken();
			tt.token = tokens.get(t);
			tt.conf =  ms.confidences[t];
			tt.tag = model.labelVocab.name( ms.labels[t] );
			if(tt.tag.equals("HT"))
				tt.tag = "NNP";
			if(tt.tag.equals("RT"))
				tt.tag = "NN";
			if(tt.tag.equals("URL"))
				tt.tag = "NNP";
			if(tt.token.equals("``")){
				tt.tag = "``";
				tt.conf = 0.99;
			}
			if(tt.token.equals("$")){
				tt.tag = "$";
				tt.conf = 0.99;
			}
			if(tt.token.equals("-LRB-")){
				tt.tag = "-LRB-";
				tt.conf = 0.99;
			}
			if(tt.token.equals("-RRB-")){
				tt.tag = "-RRB-";
				tt.conf = 0.99;
			}
			if(tt.token.equals("''")){
				tt.tag = "''";
				tt.conf = 0.99;
			}
			if(tt.token.equals("''")){
				tt.tag = "CC";
				tt.conf = 0.99;
			}
			if(tt.token.equals("Urlname") || tt.token.equals("Username")){
				tt.tag = "NNP";
				tt.conf = 0.99;
			}

			taggedTokens.add(tt);
		}
		return taggedTokens;
	}
	
	public static class TaggedToken {
		public String token;
		public String tag;
		public double conf;
	}
}