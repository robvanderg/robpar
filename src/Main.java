import earley.CombinedParser;
import grammar.Grammar;
import grammar.GrammarReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
// "eng_web_tbk", ".tree" 
// "ontonotes", ".parse"
// "Web2.0Dataset", "_gold"
// "penn", ".mrg" //TODO error with words/pos tags

import tagger.TagReader;

public class Main {
	static Grammar myGrammar;
	
	public static void main(String[] args) { 
		//myGrammar= new GrammarReader().generate(new File("data/train/all_train"));
		//myGrammar.write("data/gram.ser");
		//new TagReader(myGrammar.getConsts(), new File("data/train/all_train")).write("data/pos.ser");
		
		myGrammar = new Grammar("data/gram.ser");

		parseCorpus("data/dev/twitterdev_tokens", "own_output");
		//eval("twitterdev_output", "data/dev/twitterdev_gold");
	}
	
	private static void parseCorpus(String testSet, String outputPath){
		CombinedParser myParser = new CombinedParser(myGrammar);
		long corpStartTime = System.nanoTime();

		int counter = 1;
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath)));
			bw.close();
			BufferedReader br = new BufferedReader(new FileReader(testSet));

			String line = "";
			while ((line = br.readLine()) != null){
				long sentStartTime = System.nanoTime();
				
				String result = myParser.parse(line);
				
				bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath, true)));
				System.out.println("\n== " + counter + ".\t" + line + "\t==");								
				System.out.println(result);
				System.out.println("Parsing sentence took: " + (System.nanoTime() - sentStartTime)/1000000000.0 + " seconds.");
				
				bw.write(result + "\n");
				bw.close();

				counter++;
			}
			br.close();
		} catch (IOException e) {
			System.err.println(e);
		}

		double secondsTook = (System.nanoTime() - corpStartTime)/1000000000.0;
		System.out.println("Parsing the whole corpus took " + secondsTook + " seconds.");
		System.out.println("For a total of " + counter + " sentences, this took an average of " + secondsTook / counter  + " per sentence.");
	}

	@SuppressWarnings("unused")
	private static void parseCorpusWithGoldPos(String testSet, String goldPos, String outputPath){
		CombinedParser myParser = new CombinedParser(myGrammar);
		long corpStartTime = System.nanoTime();

		int counter = 1;
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath)));
			bw.close();
			BufferedReader br = new BufferedReader(new FileReader(testSet));
			BufferedReader brPos = new BufferedReader(new FileReader(goldPos));

			String line = "";
			while ((line = br.readLine()) != null){
				long sentStartTime = System.nanoTime();
				
				bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath, true)));

				System.out.println("\n== " + counter + ".\t" + line + "\t==");
				
				String posLine = "";
				ArrayList<String> pos = new ArrayList<String>();
				while((posLine = brPos.readLine()) != null){
					if(posLine.length() == 0)
						break;
					else
						pos.add(posLine.substring(posLine.indexOf('\t') + 1));
				}
				
				String result = myParser.parse(line, pos);
				System.out.println(result);
				bw.write(result + "\n");
				bw.close();

				System.out.println("Parsing sentence took: " + (System.nanoTime() - sentStartTime)/1000000000.0 + " seconds.");
				counter++;
			}
			br.close();
			brPos.close();
		} catch (IOException e) {
			System.err.println(e);
		}

		double secondsTook = (System.nanoTime() - corpStartTime)/1000000000.0;
		System.out.println("Parsing the whole corpus took " + secondsTook + " seconds.");
		System.out.println("For a total of " + counter + " sentences, this took an average of " + secondsTook / counter  + " per sentence.");
	}

	@SuppressWarnings("unused") // no evaluation yet!
	private static void eval(String parsed, String gold) {
		String s;
		Process p;
		try {
			String command = "data/EVALB/evalb -p " + "data/EVALB/COLLINS.prm" + " " +gold + " " + parsed;
			System.out.println(command);
			p = Runtime.getRuntime().exec(command);
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((s = br.readLine()) != null)
				System.out.println(s);
			p.waitFor();
			System.out.println ("exit: " + p.exitValue());
			p.destroy();
		} catch (Exception e) {}
	}
	
	
}
