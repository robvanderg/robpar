package grammar;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Grammar implements Serializable{
	private static final long serialVersionUID = 4911599787929046325L;
	/* A list with all constituents in the training grammar, the index is the id!*/
	private ArrayList<String> consts; 

	/* A 3d array that contains lhs, rhs, count of all rules in the training trees*/
	private int[][][] rules;
	
	public Grammar(int[][][] rules, ArrayList<String> consts){
		this.consts = consts;
		this.rules = rules;
	}
	
	public Grammar(String filePath){
		this.read(filePath);
	}
	
	public boolean isPos(int id){
		return id >= rules.length;
	}

	public int[][] getRulesForCat(int constId) {
		return rules[constId];
	}

	public int toId(String constituent){
		return this.consts.indexOf(constituent);
	}
	
	public String toConstituent(int constId){
		if (constId == 99999){
			return "word"; //TODO, kan netter!
		}
		return consts.get(constId);
	}

	@SuppressWarnings("unchecked")
	private void read(String filePath){
		try{
			ObjectInput input = new ObjectInputStream (new BufferedInputStream(new FileInputStream(filePath)));
			rules = (int[][][])input.readObject();
			consts = (ArrayList<String>)input.readObject();
			input.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void write(String filePath){
		try {
			ObjectOutput output = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filePath)));
			output.writeObject(rules);
			output.writeObject(consts);
			output.close();
			System.out.println(filePath + " written");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getConsts(){
		return consts;
	}
}
