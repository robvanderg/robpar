package grammar;

import java.util.ArrayList;
import java.util.HashMap;

public class GrammarGenerator {
	static ArrayList<String> consts = new ArrayList<String>();

	public static Grammar generate(HashMap<String, HashMap<ArrayList<String>, Integer>> countList){
		/* First add lhs, so rules will be on top of the list */
		for (String lhs: countList.keySet())
			consts.add(lhs);

		/* Now add pos tags */
		for (HashMap<ArrayList<String>, Integer> rulesHash: countList.values()) // lhs
			for (ArrayList<String> rhs: rulesHash.keySet()) //rhs
				for(String constituent: rhs) // constituent
					if (!consts.contains(constituent)) // only new constituents
						consts.add(constituent);
		
		/* Initiate rules */
		int[][][] rules = new int[countList.size()][][];
		/* Fill rules! */
		for (int constId = 0; constId < rules.length; constId++){
			HashMap<ArrayList<String>, Integer> lhsData = countList.get(consts.get(constId));

			if(!countList.containsKey(consts.get(constId))){
				lhsData = new HashMap<ArrayList<String>, Integer>();
			}
			
			int lhsCount = 0;

			for(ArrayList<String> rhsString: lhsData.keySet())
				lhsCount += lhsData.get(rhsString);

			int[][] rhsRules = new int[lhsData.size()][];
			int rhsIndex = 0;
			for(ArrayList<String> rhsString: lhsData.keySet())
				rhsRules[rhsIndex++] = toInt(lhsCount, lhsData.get(rhsString), rhsString);
			rules[constId] = rhsRules;
		}
		consts.add("Y");

		return new Grammar(rules, consts);
	}

	private static int[] toInt(int lhsCount, int rhsCount, ArrayList<String> rhsString) {
		int[] rhsInt = new int[rhsString.size() + 2];
		rhsInt[0] = lhsCount;
		rhsInt[1] = rhsCount;
		for (int rhsIndex = 0; rhsIndex < rhsString.size(); rhsIndex++)
			rhsInt[rhsIndex + 2] = consts.indexOf(rhsString.get(rhsIndex));
		return rhsInt;
	}
}