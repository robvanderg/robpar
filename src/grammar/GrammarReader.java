package grammar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class GrammarReader {
	/* A list that counts the occurences of the rules, this data will later be put in int[][][] rules for efficiency. */
	private HashMap<String, HashMap<ArrayList<String>, Integer>> countList;
			
	public Grammar generate(File trainFile){
		return generate(new File[]{trainFile});
	}

	/* First add all counts, then return a grammar based on these counts */
	public Grammar generate(File[] trainFiles){
		this.countList = new HashMap<String, HashMap<ArrayList<String>, Integer>>();
		for (File trainFile: trainFiles)
			this.addCounts(trainFile);
		return GrammarGenerator.generate(countList);
	}
	
	/* Reads the file character by character, save the nodes in chronological order in depthList including their depth */
	private void addCounts(File parseTrees){
		ArrayList<DepthItem> depthList = new ArrayList<DepthItem>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(parseTrees));
			int bracketCount = 0;
			int cInt;
			String word = "";
			while((cInt = br.read()) > 0) {
				char character = (char) cInt;
				//TODO make switch!
				if (character == ' '){
					if (word.trim().length() > 0)
						depthList.add(new DepthItem(bracketCount, word, false));
					word = "";
				}
				else if(character == ')'){ //POS!!
					if (word.trim().length() > 0)
						depthList.add(new DepthItem(bracketCount + 1, word, true));
					bracketCount--;
					if (bracketCount == 0){
						addRules(depthList);
						depthList = new ArrayList<DepthItem>();
					}
					word = "";
				}
				else if (character == '(')
					bracketCount++;
				else if(character != '\n')
					word += character;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* Extracts rules from a depthList, calls addRule when a rule is found */
	private void addRules(ArrayList<DepthItem> depthList) {
		for (int nodeIndex = 0; nodeIndex < depthList.size() ; nodeIndex++){
			int ownDepth = depthList.get(nodeIndex).depth;
			ArrayList<String> rhs = new ArrayList<String>();
			String lhs  = depthList.get(nodeIndex).tag;
			
			boolean isRule = false;
			for (int itrIndex = nodeIndex+1; itrIndex < depthList.size(); itrIndex++)
				if (depthList.get(itrIndex).depth == ownDepth)
					break;
				else if (depthList.get(itrIndex).depth == ownDepth + 1){
					if(depthList.get(itrIndex).isWord == true)
						break;
					rhs.add(this.cleanCons(depthList.get(itrIndex).tag));
					isRule = true;
				}
			
			if (isRule)
				this.addRule(this.cleanCons(lhs), rhs);
		}
	}
	
	/* Adds a rule to the countList, or increases its count */
	private void addRule(String lhs, ArrayList<String> rhs) {
		if(rhs.size() > 8 || rhs.size() < 1) // skip long rules, because they slow down the parser and dont exist in the dev data
			return;
		for(int i = 0; i < rhs.size(); i++) // Removal of epsilons
			if(rhs.get(i).contains("-NONE-"))
				if (rhs.size() <= 1)
					return; 
				else
					rhs.remove(i);
		
		if(countList.get(lhs) == null)
			countList.put(lhs, new HashMap<ArrayList<String>, Integer>());
		
		if (countList.get(lhs).containsKey(rhs))
			countList.get(lhs).put(rhs, countList.get(lhs).get(rhs)+1);
		else
			countList.get(lhs).put(rhs, 1);
	}
	
	/* Clean a constituent for efficiency reasons */
	private String cleanCons(String cons){
		int dashLoc = cons.indexOf('-');
		if (dashLoc > 0)
			cons = cons.substring(0, dashLoc);
		int eqLoc = cons.indexOf('=');
		if (eqLoc > 0)
			cons = cons.substring(0, eqLoc);

		if (cons.equals("META") || cons.equals("EDITED"))
			return "X";
		else if (cons.equals("NFP"))
			return ":";
		else if (cons.equals("ADD"))
			return "NNP";
		else if (cons.equals("HYPH"))
			return ":";
		else if (cons.equals("NML"))
			return "NP";
		return cons;
	}
}

/* helper to find the rules! */
class DepthItem{ 
	int depth;
	String tag;
	boolean isWord;
	public DepthItem(int depth, String tag, boolean isWord){
		this.depth = depth;
		this.tag = tag;
		this.isWord = isWord;
	}
}